//
//  ViewController.swift
//  blescanner
//
//  Created by Jason Coulls on 2019-08-01.

import Cocoa
import CoreBluetooth

class ViewController: NSViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    //This is the Core Bluetooth Manager
    private var manager: CBCentralManager!
    
    //This is the array of devices IDs we've seen.
    //It's just used to not report same device twice.
    private var arrDevicesFound:[String] = []
    
    //This is the attay where we store references to peripherals we are connected to.
    private var connectedDevice:CBPeripheral?
    
    //This is the reference to the textview on the UI.
    @IBOutlet weak var txtOutput: NSTextView?
    
    //This is just a global string used to hold the text we'll display.
    private var strResult : String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //This initialises the bluetooth manager, which will trigger
        //the centralManagerDidUpdateState function
        self.manager = CBCentralManager(delegate: self, queue: nil)
    }

    override func viewWillDisappear() {
        //Stop scanning.
        self.manager.stopScan()
    }
    
    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    func logThis(strToLog : String) {
        
        // Log the basic string to the console (this will auto-add a new line).
        print(strToLog)
        
        // Add a new line to the results string, before logging it in the UI.
        self.strResult = self.strResult + strToLog + "\n"
        self.txtOutput?.string = self.strResult
    
    }
    
    
    // Bluetooth routines below here.
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
    
        switch (central.state) {
        case .poweredOff:
            self.logThis(strToLog: "Powered Off")
        case .poweredOn:
            // Now we are in an ON state, start scanning for peripherals.
            self.logThis(strToLog: "Powered On - Initiating Scan")
            self.manager.scanForPeripherals(withServices: nil, options: nil)
        case .unauthorized:
            self.logThis(strToLog: "Unauthorized")
        case .unknown:
            self.logThis(strToLog: "Unknown")
        case .unsupported:
            self.logThis(strToLog: "Unsupported")
        default:
            self.logThis(strToLog: "Default")
        }
    }
    
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {

        let deviceID : String = peripheral.identifier.uuidString

        if self.arrDevicesFound.contains(deviceID){
            //We've seen this device before.  Ignore it, and exit.
            return
        }
        
        if self.connectedDevice != nil{
            //We are still connected to a device, so come back shortly.
            //Discoverable devices will rebroadcast their advertisement again.
            return
        }

        //If we get here, we've never seen this device before.
        let name : String? = peripheral.name

        if name == nil {
            //Wait. The device may broadcast it's name in subsequent advertisements.
            return
        } else {
            //Record this device ID
            self.arrDevicesFound.append(deviceID)
            //Now store a reference to this peripheral. We'll need it later.
            self.connectedDevice = peripheral
            self.connectedDevice?.delegate = self;
            //Grab the name.
            self.logThis(strToLog: "\n--- New Device Found ---\n")
            self.logThis(strToLog: "Name: \(name!)")
            self.logThis(strToLog: "Details: \(peripheral.description)")
            //Now try to connect to it.
            manager.connect(self.connectedDevice!, options: nil)
        }
    }
    
    func disconnect(peripheral:CBPeripheral){
        self.logThis(strToLog: "Auto-disconnecting from peripheral \(peripheral)")
        self.manager.cancelPeripheralConnection(self.connectedDevice!)
    }

    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        //Now we've connected, set up an auto-disconnect for 5 seconds from now.
        let executeTime = DispatchTime.now() + .seconds(5)
        DispatchQueue.main.asyncAfter(deadline: executeTime) {
            self.connectedDevice = peripheral
            self.disconnect(peripheral: self.connectedDevice!)
        }
        //In the meantime, try to discover the services on the peripheral.
        peripheral.discoverServices(nil)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service : CBService in peripheral.services! {
            self.logThis(strToLog: "Discovered Service: \(service.description)")
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for characteristic:CBCharacteristic in service.characteristics!{
            self.logThis(strToLog: "Discovered Characteristic: \(characteristic.description)")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        //Remove our reference to any device we previously connected to.
        self.connectedDevice = nil
    }
}



