//
//  AppDelegate.swift
//  blescanner
//
//  Created by Jason Coulls on 2019-08-01.
//  Copyright © 2019 Jason Coulls. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

